using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ModTitle("Tombstones")] // The mod name.
[ModDescription("Simply add tomb where you died.")] // Short description for the mod.
[ModAuthor("TechDivers")] // The author name of the mod.
[ModIconUrl("download1587.mediafire.com/nnxz0dgdxlfg/hkebxqkswb5e2o5/ico.png")] // An icon for your mod. Its recommended to be 128x128px and in .jpg format.
[ModWallpaperUrl("download1645.mediafire.com/ogk98e68zqxg/avi6lmytpal5mt2/wallpaper.png")] // A banner for your mod. Its recommended to be 330x100px and in .jpg format.
[ModVersionCheckUrl("Version File Url")] // This is for update checking. Needs to be a .txt file with the latest mod version.
[ModVersion("1.0.0")] // This is the mod version.
[RaftVersion("Update Name")] // This is the recommended raft version.
[ModIsPermanent(false)] // If your mod add new blocks, new items or just content you should set that to true. It loads the mod on start and prevents unloading.
public class Tombstones : Mod
{
    Network_Player player;

    Vector3 deathPos;

    AssetBundle asset;
    private int nbrTombs;

    //Tombs
    GameObject tomb;
    GameObject tomb_W;

    //private List<ItemInstance> itemsAtDeath = new List<ItemInstance>();
    //private List<ItemInstance> itemsAtDeathToReequip = new List<ItemInstance>();

    //private Dictionary<int, DeathItems> itemsCombined = new Dictionary<int, DeathItems>();
    public List<DeathItems> deathItemList = new List<DeathItems>();

    public IEnumerator Start()
    {
        RConsole.Log("Tombstones has been loaded!");
        player = RAPI.getLocalPlayer();
        AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync("mods/ModData/Tombstones/tombstones.assets");
        yield return request;
        asset = request.assetBundle;

        tomb = asset.LoadAsset<GameObject>("tombstone_air");
        tomb_W = asset.LoadAsset<GameObject>("tombstone_water");
    }


    public void OnModUnload()
    {
        asset.Unload(true);
        RConsole.Log("Tombstones has been unloaded!");
        Destroy(gameObject);
    }

    public override void LocalPlayerEvent_Death(Vector3 deathPosition)
    {
        deathPos = deathPosition;
        var itemsAtDeath = new List<ItemInstance>();
        var itemsAtDeathToReequip = new List<ItemInstance>();

        foreach (Slot slot in player.Inventory.allSlots)
        {
            if (!slot.IsEmpty)
            {
                itemsAtDeath.Add(slot.itemInstance);
            }
        }

        foreach (Slot_Equip slotequip in player.Inventory.equipSlots)
        {
            if (!slotequip.IsEmpty)
            {
                itemsAtDeathToReequip.Add(slotequip.itemInstance);
            }
        }

        deathItemList.Add(new DeathItems(itemsAtDeath, itemsAtDeathToReequip));
    }

    public override void LocalPlayerEvent_Respawn()
    {
        player.Inventory.Clear();

        if(!(deathItemList[nbrTombs].itemsAtDeath.Count == 0 && deathItemList[nbrTombs].itemsAtDeathToReequip.Count == 0))
        {
            var sTomb = tomb;

            if (deathPos.y > 0.2f)
            {
                sTomb = Instantiate(tomb);
            }
            else
            {
                sTomb = Instantiate(tomb_W);
            }

            sTomb.AddComponent<TombstoneComp>();
            sTomb.transform.position = deathPos;
            sTomb.GetComponent<TombstoneComp>().player = player;
            sTomb.GetComponent<TombstoneComp>().mod = this.gameObject;
            sTomb.GetComponent<TombstoneComp>().index = nbrTombs;

            var lookPos = transform.position - sTomb.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            sTomb.transform.rotation = rotation;
            sTomb.transform.Rotate(0, 90, 0);

            nbrTombs++;
        }
        RConsole.Log(""+deathItemList.Count);
    }

    public struct DeathItems
    {
        public readonly List<ItemInstance> itemsAtDeath;
        public readonly List<ItemInstance> itemsAtDeathToReequip;

        public DeathItems(List<ItemInstance> itemsAtDeath, List<ItemInstance> itemsAtDeathToReequip)
        {
            this.itemsAtDeath = itemsAtDeath;
            this.itemsAtDeathToReequip = itemsAtDeathToReequip;
        }
    }
}

public class TombstoneComp : MonoBehaviour
{
    public int index;
    public GameObject mod;
    public float a = 0;
    public Network_Player player;

    private List<ItemInstance> customItemsAtDeath = new List<ItemInstance>();
    private List<ItemInstance> customItemsAtDeathToReequip = new List<ItemInstance>();

    public void Start()
    {
        customItemsAtDeath = mod.GetComponent<Tombstones>().deathItemList[index].itemsAtDeath;
        customItemsAtDeathToReequip = mod.GetComponent<Tombstones>().deathItemList[index].itemsAtDeathToReequip;

        if (!(transform.position.y < 0.2f))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity))
            {
                transform.position = hit.point;
            }
        }
    }

    public void Update()
    {
        if (this.gameObject.transform.position.y < -0.1f)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, new Vector3(this.transform.position.x, 0, this.transform.position.z), Time.fixedDeltaTime * 0.3f);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (a >= 1)
            {
                Destroy(this.gameObject);
                var i = 0;

                foreach (Slot_Equip slot_Equip in player.Inventory.equipSlots)
                {
                    if (i <= customItemsAtDeathToReequip.Count - 1)
                    {
                        slot_Equip.SetItem(mod.GetComponent<Tombstones>().deathItemList[index-1].itemsAtDeathToReequip[i]);
                    }
                    i++;
                }

                foreach (var item in customItemsAtDeath)
                {
                    player.Inventory.AddItem(item);
                }
            }
            else{ a++; }
        }
    }
}
