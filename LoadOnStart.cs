﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Harmony;

[ModTitle("<align=\"center\">LoadOnStart")]
[ModDescription("Loads and keeps the landmarks asset bundle loaded to improve loading times")]
[ModAuthor(". Marsh.Mello .")]
[ModIconUrl("https://cdn.discordapp.com/attachments/374545369998032899/656779270932135936/LOS.jpg")]
[ModWallpaperUrl("https://cdn.discordapp.com/attachments/374545369998032899/656778882996895754/LoadOnStart.png")]
[ModVersion("1.2.2")]
[RaftVersion("Update 10.07")]
[ModVersionCheckUrl("https://raftmodding.com/api/v1/mods/los/version.txt")] 
[ModIsPermanent(true)]
public class LoadOnStart : Mod
{
    private static string prefix = "<color=#32CD32>LoadOnStart</color>: ";
    private HarmonyInstance harmony;
    private readonly string harmonyID = "com.raftmodding.marsh.loadonstart";
    private AssetBundleManager manager;
    private RNotify notify;
    public void Start()
    {
        harmony = HarmonyInstance.Create(harmonyID);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
        Log("Loaded");

        manager = FindObjectOfType<AssetBundleManager>();
        notify = FindObjectOfType<RNotify>();
        StartCoroutine(CustomLoad());
    }

    private IEnumerator CustomLoad()
    {
        RNotification notification = notify.AddNotification(RNotify.NotificationType.spinning, "<color=#32CD32>Loading Landmarks");
        Traverse traverse = Traverse.Create(manager);
        bool startedLoading = (bool)traverse.Field("startedLoadingBundle").GetValue();
        Log(manager.IsBundleLoaded + "|" + startedLoading);
        if (manager.IsBundleLoaded || startedLoading)
        {
            yield break;
        }
        traverse.Field("startedLoadingBundle").SetValue(true);
        AssetBundleCreateRequest assetBundleRequest = null;
        try
        {
            Log("made Request");
            assetBundleRequest = AssetBundle.LoadFromFileAsync(Application.streamingAssetsPath
                + "/AssetBundles/" + "landmarks");
        }
        catch (Exception message)
        {
            LogError(message);
        }
        yield return assetBundleRequest;
        traverse.Field("bundleLandmark").SetValue(assetBundleRequest.assetBundle);
        if (traverse.Field("bundleLandmark").GetValue() == null)
        {
            yield break;
        }
        traverse.Field("startedLoadingBundle").SetValue(false);
        notification.Close();
        notify.AddNotification(RNotify.NotificationType.normal, text: "<color=#32CD32>Loaded Landmarks", closeDelay: 3);
        yield break;
    }

    public void OnModUnload()
    {
        harmony.UnpatchAll(harmonyID);
        Log("Unloaded!");
        Destroy(gameObject);
    }

    public static void Log(object message)
    {
        Debug.Log(prefix + message);
    }
    public static void LogError(object message)
    {
        Debug.LogError(prefix + message);
    }
}

[HarmonyPatch(typeof(AssetBundleManager))]
[HarmonyPatch("UnloadBundle")]
class AssetBundleManager_Unload
{
    static bool Prefix(bool removeObjects)
    {
        return false;
    }
}
