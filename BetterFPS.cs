using Harmony;
using System.Collections;
using System.Reflection;
using UltimateWater;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.AzureSky;

[ModTitle("BetterFPS")]
[ModDescription("BetterFPS Allows you to disable and fix some heavy game functions.")]
[ModAuthor("TeKGameR")]
[ModIconUrl("https://i.imgur.com/niLkYsv.jpg")]
[ModWallpaperUrl("https://i.imgur.com/GhiNXY0.jpg")]
[ModVersionCheckUrl("https://www.raftmodding.com/TeKGameRMods/versions/betterfps.txt")]
[ModVersion("2.1")]
[RaftVersion("Update 9")]
public class BetterFPS : Mod
{
    private bool ShowMenu;
    private AssetBundle asset;
    private GameObject menu;
    HarmonyInstance harmonyInstance;
    string harmonyId = "fr.tekgamer.betterfps";

    public static bool DisableWaterRendering = false;
    public static bool DisableRaftCollisions = false;
    public static bool DisableBlockBreakingLag = false;
    public static bool DisableAchievements = false;
    public static bool DisableNetworking = false;
    public static bool DisableAI = false;
    public static bool DisableSkyController = false;

    public IEnumerator Start()
    {
        harmonyInstance = HarmonyInstance.Create(harmonyId);
        harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
        if (GameObject.Find("BetterFPSMenuCanvas")){
            Destroy(GameObject.Find("BetterFPSMenuCanvas"));
        }

        using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle("https://www.raftmodding.com/TeKGameRMods/assets/betterfps2.assets"))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                RConsole.LogError("An error occured while downloading BetterFPS assets!\n" + uwr.error);
                yield break;
            }
            else
            {
                asset = DownloadHandlerAssetBundle.GetContent(uwr);
            }
        }

        menu = Instantiate<GameObject>(asset.LoadAsset<GameObject>("BetterFPSMenuCanvas"), Vector3.zero, Quaternion.identity);
        DontDestroyOnLoad(menu);
        menu.name = "BetterFPSMenuCanvas";
        menu.transform.Find("Background").Find("CloseBtn").GetComponent<Button>().onClick.AddListener(CloseBtn);
        GameObject.Find("BetterFPS_DisableWater").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableWaterRendering = value;
            Water water = Object.FindObjectOfType<Water>();
            water.RenderingEnabled = !DisableWaterRendering;
        });
        GameObject.Find("BetterFPS_DisableRaftCollision").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableRaftCollisions = value;
            GameModeValueManager.GetCurrentGameModeValue().raftSpecificVariables.handleRaftCollision = !DisableRaftCollisions;
            ComponentManager<RaftCollisionManager>.Value.Initialize();
        });
        GameObject.Find("BetterFPS_DisableDestroyingLag").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableBlockBreakingLag = value;
        });
        GameObject.Find("BetterFPS_DisableAchievements").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableAchievements = value;
        });
        GameObject.Find("BetterFPS_DisableObjectSpawners").SetActive(false);
        GameObject.Find("BetterFPS_DisableNetworking").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableNetworking = value;
        });
        GameObject.Find("BetterFPS_DisableAI").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableAI = value;
        });
        GameObject.Find("BetterFPS_DisableSkyController").GetComponent<Toggle>().onValueChanged.AddListener(delegate (bool value)
        {
            DisableSkyController = value;
            ComponentManager<AzureSkyController>.Value.enabled = !DisableSkyController;
        });
        menu.GetComponent<Canvas>().enabled = false;
        RConsole.Log("BetterFPS loaded!");
    }

    public void Update()
    {
        if (Semih_Network.InLobbyScene && ShowMenu)
        {
            CloseBtn();
            return;
        }

        if (!Semih_Network.InLobbyScene)
        {
            if (Input.GetKeyDown(KeyCode.F7))
            {
                ShowMenu = !ShowMenu;
                RAPI.ToggleCursor(ShowMenu);
                this.menu.GetComponent<Canvas>().enabled = ShowMenu;
            }
        }
    }

    public void OnModUnload()
    {
        asset.Unload(true);
        harmonyInstance.UnpatchAll(harmonyId);
        RConsole.Log("BetterFPS has been unloaded!");
        Destroy(gameObject);
    }

    public void CloseBtn()
    {
        ShowMenu = false;
        RAPI.ToggleCursor(ShowMenu);
        menu.GetComponent<Canvas>().enabled = ShowMenu;
    }
}

[HarmonyPatch(typeof(BlockCreator))]
[HarmonyPatch("DestroyBlock")]
public static class HarmonyPatch_DisableDestroyingLag
{
    private static bool Prefix(BlockCreator __instance, Block block)
    {
        if (BetterFPS.DisableBlockBreakingLag)
        {
            if (block != null)
            {

                if (BlockCreator.RemoveBlockCallStack != null)
                {
                    BlockCreator.RemoveBlockCallStack(block);
                }
                if (block.networkedBehaviour != null)
                {
                    NetworkUpdateManager.RemoveBehaviour(block.networkedBehaviour);
                }
                Object.DestroyImmediate(block.gameObject);
            }
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AchievementHandler))]
[HarmonyPatch("Update")]
public static class HarmonyPatch_DisableAchievements
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAchievements)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(Semih_Network))]
[HarmonyPatch("Update")]
public static class HarmonyPatch_DisableNetworking
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableNetworking)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AI_StateMachine))]
[HarmonyPatch("Update")]
public static class HarmonyPatch_DisableAI1
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAI)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AI_StateMachine))]
[HarmonyPatch("Update")]
public static class HarmonyPatch_DisableAI2
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAI)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AI_Movement))]
[HarmonyPatch("Update")]
public static class HarmonyPatch_DisableAI3
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAI)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AI_Movement))]
[HarmonyPatch("LateUpdate")]
public static class HarmonyPatch_DisableAI4
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAI)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AI_NetworkBehaviour))]
[HarmonyPatch("Update")]
public static class HarmonyPatch_DisableAI5
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAI)
        {
            return false;
        }
        return true;
    }
}

[HarmonyPatch(typeof(AI_Movement_Lean))]
[HarmonyPatch("LateUpdate")]
public static class HarmonyPatch_DisableAI6
{
    private static bool Prefix()
    {
        if (BetterFPS.DisableAI)
        {
            return false;
        }
        return true;
    }
}