using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

[ModTitle("No Pillars")] // The mod name.
[ModDescription("Removes the need for pillars.")] // Short description for the mod.
[ModAuthor("TeigRolle")] // The author name of the mod.
[ModIconUrl("https://i.imgur.com/l4SHPuS.png")] // An icon for your mod. Its recommended to be 128x128px and in .jpg format.
[ModWallpaperUrl("https://i.imgur.com/kmcefTI.png")] // A banner for your mod. Its recommended to be 330x100px and in .jpg format.
[ModVersionCheckUrl("https://raftmodding.com/api/v1/mods/nopillars/version.txt")] // This is for update checking. Needs to be a .txt file with the latest mod version.
[ModVersion("0.1.3")] // This is the mod version.
[RaftVersion("Update Name")] // This is the recommended raft version.
[ModIsPermanent(true)] // If your mod add new blocks, new items or just content you should set that to true. It loads the mod on start and prevents unloading.
public class NoPillars : Mod
{


    private readonly string HARMONY_ID = "TeigRolle.NoPillars";
    private HarmonyInstance harmonyInstance;

    // The Start() method is being called when your mod gets loaded.
    public void Start()
    {
        harmonyInstance = HarmonyInstance.Create(HARMONY_ID);
        harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());

        RConsole.Log("NoPillars has been loaded!");
    }

    

    public void ChangeBlock()
    {

       
        GameObject newFloorColliderPrefab;
        GameObject newFloorTriangularColliderPrefab;

        string pathbcm = "blockcollisionmasks/blockcollisionmask_pillar";
        SO_BlockCollisionMask pillarMask = Traverse.Create(Resources.Load<ScriptableObject>(pathbcm)).GetValue<SO_BlockCollisionMask>();

        

        string pathbqt = "blockquadtype/quad_floor_empty";
        SO_BlockQuadType blockQuadType = Traverse.Create(Resources.Load<ScriptableObject>(pathbqt)).GetValue<SO_BlockQuadType>();

        //Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(Resources.LoadAll<Item_Base>("IItems").ToList<Item_Base>());
        List<Item_Base> list = Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").GetValue<List<Item_Base>>();

        foreach (Item_Base item_Base in list) {
            if (item_Base.UniqueIndex == 1 || item_Base.UniqueIndex == 191) {
                //item_Base.settings_buildable.GetBlockPrefab(0).gameObject.AddComponent<StableComponent>();
                Destroy(item_Base.settings_buildable.GetBlockPrefab(0).gameObject.GetComponent<StableComponent>());
                item_Base.settings_buildable.GetBlockPrefab(0).blockCollisionMask = pillarMask;
            }
        }

        Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(list);

        Item_Base floorItem           = ItemManager.GetItemByIndex(1);
        Item_Base floorTriangularItem = ItemManager.GetItemByIndex(191);

        string colliderPrefabPoolName           = floorItem          .settings_buildable.GetBlockPrefab(0).GetColliderPrefabPoolName();
        string colliderPrefabPoolNameTriangular = floorTriangularItem.settings_buildable.GetBlockPrefab(0).GetColliderPrefabPoolName();

        ObjectPool floorPool           = GameObject.FindObjectOfType<PoolManager>().GetPoolByName(colliderPrefabPoolName);
        ObjectPool floorTriangularPool = GameObject.FindObjectOfType<PoolManager>().GetPoolByName(colliderPrefabPoolNameTriangular);

        //Create for floor and apply to triangular later
        newFloorColliderPrefab = Instantiate(floorPool.ObjectPrefab);
        Destroy(newFloorColliderPrefab.GetComponent<StableComponent>());

        BlockSurface blockSurface = new BlockSurface();
        blockSurface.surfaceType = SurfaceType.All;
        blockSurface.dpsType = DPS.Ceiling;

        GameObject[] newFloorColliders = new GameObject[] {
            new GameObject("BuildquadCeilingNewR"),
            new GameObject("BuildquadCeilingNewL"),
            new GameObject("BuildquadCeilingNewB"),
            new GameObject("BuildquadCeilingNewF")
        };

        for (int i = 0; i < newFloorColliders.Length; i++) {
            newFloorColliders[i].layer = 8;
            newFloorColliders[i].transform.parent = newFloorColliderPrefab.transform;
            newFloorColliders[i].transform.localEulerAngles = new Vector3(0.0f, 90.0f, 0.0f);
            newFloorColliders[i].AddComponent<BoxCollider>().isTrigger = true;
            newFloorColliders[i].GetComponent<BoxCollider>().size = new Vector3(1.45f, 0.2f, 1.45f);
            newFloorColliders[i].AddComponent<BlockQuad>();
            newFloorColliders[i].GetComponent<BlockQuad>().quadType = blockQuadType;
            newFloorColliders[i].GetComponent<BlockQuad>().acceptableBuildSides = new BlockSurface[] { blockSurface };
            newFloorColliders[i].GetComponent<BlockQuad>().snapToQuadRotation = true;
        }

        newFloorColliders[0].transform.localPosition = new Vector3(1.5f, 0.0f, 0.0f);
        newFloorColliders[1].transform.localPosition = new Vector3(-1.5f, 0.0f, 0.0f);
        newFloorColliders[2].transform.localPosition = new Vector3(0.0f, 0.0f, 1.5f);
        newFloorColliders[3].transform.localPosition = new Vector3(0.0f, 0.0f, -1.5f);

        ChangeFloorPrefab(1, newFloorColliderPrefab);
        ChangeFloorPrefab(191, newFloorColliderPrefab);

        /*
        newFloorTriangularColliderPrefab = Instantiate(newFloorColliderPrefab);

        Destroy(newFloorTriangularColliderPrefab.transform.Find("BuildquadCeilingNewF").gameObject);
        Destroy(newFloorTriangularColliderPrefab.transform.Find("BuildquadCeilingNewR").gameObject);
        
         ChangeFloorPrefab(191, newFloorTriangularColliderPrefab);
         */

    }

    static void ChangeFloorPrefab(int blockIndex, GameObject colliderPrefab) {
        Item_Base floorItem = ItemManager.GetItemByIndex(blockIndex);
        string colliderPrefabPoolName = floorItem.settings_buildable.GetBlockPrefab(0).GetColliderPrefabPoolName();
        ObjectPool floorPool = GameObject.FindObjectOfType<PoolManager>().GetPoolByName(colliderPrefabPoolName);
        Traverse.Create(floorPool).Field("objectPrefab").SetValue(colliderPrefab);
        GameObject[] objs = floorPool.Objects.ToArray();

        for (int i = 0; i < objs.Length; i++) {
            floorPool.Objects.Remove(objs[i]);
            Destroy(objs[i]);
        }

        floorPool.Initialize();
    }
}

/*
[HarmonyPatch(typeof(BlockCreator))]
[HarmonyPatch("CanBuildBlock")]
class BlockCreatorPatch
{
    static BuildError Postfix(BuildError __result, Block block)
    {
        RConsole.Log(__result.ToString());
        return __result;
    }
}
*/

[HarmonyPatch(typeof(PoolManager))]
[HarmonyPatch("Awake")]
class PoolmanagerPatch
{
    static void Postfix(PoolManager __instance)
    {
        GameObject.FindObjectOfType<NoPillars>().ChangeBlock();
    }
}

[HarmonyPatch(typeof(StableComponent), "IsStable")]
class StableComponentPatch
{
    static bool Prefix(StableComponent __instance, ref bool __result)
    {
        Block thisBLock = __instance.GetComponent<Block>();
        if (thisBLock == null) {
            thisBLock = __instance.transform.parent.GetComponent<Block>();
        }

        if (thisBLock != null) {
            if (thisBLock.buildableItem.UniqueIndex == 1 || thisBLock.buildableItem.UniqueIndex == 191) {
                __result = true;
                return false;
            }
        }
        return true;
    }
}

