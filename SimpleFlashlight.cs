using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

[ModTitle("SimpleFlashlight")]
[ModDescription("SimpleFlashlight allows you to toggle a portable flashlight using the key F like in Garry's Mod.")]
[ModAuthor("TeKGameR")]
[ModIconUrl("https://i.imgur.com/TevTCKi.jpg")]
[ModWallpaperUrl("https://i.imgur.com/EkzoN0u.jpg")]
[ModVersionCheckUrl("https://www.raftmodding.com/TeKGameRMods/versions/simpleflashlight.txt")]
[ModVersion("2.1")]
[RaftVersion("Update 9")]
public class SimpleFlashlight : Mod
{
    Network_Player player;
    GameObject flashlight;

    public void Update()
    {
        if (player == null) { player = RAPI.getLocalPlayer(); return; }
        if (SceneManager.GetActiveScene().name != Semih_Network.GameSceneName) { return; }
        if (!player.CameraTransform.transform.Find("Flashlight")) {
            GameObject go = new GameObject("Flashlight");
            go.transform.SetParent(player.CameraTransform);
            go.transform.localPosition = new Vector3(0, 0, 0);
            go.transform.localRotation = Quaternion.identity;
            go.AddComponent<Light>();
            go.GetComponent<Light>().type = LightType.Spot;
            go.GetComponent<Light>().spotAngle = 75;
            go.GetComponent<Light>().range = 20;
            flashlight = go;
        }
        if (flashlight)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (flashlight.activeSelf)
                {
                    flashlight.SetActive(false);
                }
                else
                {
                    flashlight.SetActive(true);
                }
            }
        }
    }

    public void OnModUnload()
    {
        if (flashlight) { Destroy(flashlight); }
        RConsole.Log("SimpleFlashlight has been unloaded!");
        Destroy(gameObject);
    }
}