
# Raft.enh ─ Modpack

---

## Requirements

* [Mod Loader](https://raftmodding.com/download)

---

## Instalation

To install just drop folder `mods` in game directory and install mod loader.

---

![](https://raft-game.com/____impro/1/onewebmedia/Screenshot_4.png?etag=W%2F%2227afa0-5afaa89e%22&sourceContentType=image%2Fpng)

---

## Mod List & Uses

> You can see more information about mods on the mod loader panel

|     Mod name     |                                                                                                               Use                                                                                                              |
|:----------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Better CropPlots |                                                                      Press shift when you harvest/plant in a crop plot to harvest/plant all slots in once.                                                                     |
| TeK's Admin Tool | Press F10 and type the command tatmenu (TAT stands for TeK's Admin Tool)
|                  |Press F10 and bind the command tatmenu to a key. (Example: bind F7 tatmenu to bind it to the F7 key)
|                  |Open the ingame chat and type !adminmenu |
|   SortInventory  |                                                    Press 'Z' to have your your inventory (minus hotbar) and any chests you have open to be automatically compressed sorted.                                                    |
