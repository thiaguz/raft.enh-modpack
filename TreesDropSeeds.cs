using Harmony;
using System;
using System.Reflection;

[ModTitle("TreesDropSeeds")]
[ModDescription("Makes sure you get a seed while harvesting trees by making their drop rate ~30% per axe hit.")]
[ModAuthor("Akitake")]
[ModIconUrl("https://gitlab.com/Akitake/RaftMods/raw/master/TreesDropSeeds/TreesDropSeeds_Icon.jpg")]
[ModWallpaperUrl("https://gitlab.com/Akitake/RaftMods/raw/master/TreesDropSeeds/TreesDropSeeds_Banner.jpg")]
[ModVersionCheckUrl("https://raftmodding.com/api/v1/mods/treesdropseeds/version.txt")]
[ModVersion("1.0.1")]
[RaftVersion("Update 10.07 (4497220)")]
[ModIsPermanent(false)]
public class TreesDropSeeds : Mod
{
    public static TreesDropSeeds instance;

    // Harmony
    public HarmonyInstance harmony;
    public readonly string harmonyID = "com.gitlab.akitake.raftmods.treesdropseeds";

    // Console stuff
    public static string modColor = "#4DFFF3";
    public static string modPrefix = "[" + Utils.Colorize("TreesDropSeeds", modColor) + "] ";

    #region Start/Unload
    public void Start()
    {
        if (instance != null) { throw new Exception("TreesDropSeeds singleton was already set"); }
        instance = this;

        harmony = HarmonyInstance.Create(harmonyID);
        harmony.PatchAll(Assembly.GetExecutingAssembly());

        RConsole.Log(modPrefix + " loaded!");
    }

    public void OnModUnload()
    {
        RConsole.Log(modPrefix + " unloaded!");
        harmony.UnpatchAll(harmonyID);
        Destroy(gameObject);
    }
    #endregion

    public static void ForceSeedDropRate(PickupItem pickupItem)
    {
        SO_RandomDropper dropper = Traverse.Create(pickupItem.dropper).Field("randomDropperAsset").GetValue() as SO_RandomDropper;
        foreach (RandomDropItem item in dropper.items)
        {
            if (item.item != null)
            {
                if (item.item.UniqueName.ToLower().Contains("seed"))
                {
                    item.weight = dropper.TotalWeight * 0.3f;
                    item.spawnChance = item.weight / dropper.TotalWeight * 100f + "%";
                }
            }
        }
    }
}

#region Utils
public class Utils
{
    #region Colorize
    public static string Colorize(string text, string col)
    {
        string s = string.Concat(new string[]
        {
            "<color=",
            col,
            ">",
            text,
            "</color>"
        });
        return s;
    }
    #endregion
}
#endregion

#region Harmony Patches
[HarmonyPatch(typeof(HarvestableTree)), HarmonyPatch("Harvest")]
public class HarvestPatch
{
    private static bool Prefix(ref PickupItem ___pickupItem)
    {
        TreesDropSeeds.ForceSeedDropRate(___pickupItem);
        return true;
    }
}
#endregion