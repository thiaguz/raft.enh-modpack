using Harmony;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

[ModTitle("Stack Mod")] // The mod name.
[ModDescription("You can increase the stack sizes of all items that are stackable together or individually")] // Short description for the mod.
[ModAuthor("Loikas, Fynikoto")] // The author name of the mod.
[ModIconUrl("Icon Url")] // An icon for your mod. Its recommended to be 128x128px and in .jpg format.
[ModWallpaperUrl("https://imagehost.imageupload.net/2020/04/07/Banner.png")] // A banner for your mod. Its recommended to be 330x100px and in .jpg format.
[ModVersionCheckUrl("https://www.raftmodding.com/api/v1/mods/stack-mod/version.txt")] // This is for update checking. Needs to be a .txt file with the latest mod version.
[ModVersion("1.5")] // This is the mod version.
[RaftVersion("Update 11")] // This is the recommended raft version.
[ModIsPermanent(false)] // If your mod add new blocks, new items or just content you should set that to true. It loads the mod on start and prevents unloading.
public class StackMod : Mod {
    private readonly Predicate<Item_Base> predicate = item => item.settings_Inventory.Stackable && item.settings_Inventory.StackSize > 1;
    private readonly Action<Item_Base, StackModSize> setStackSize =
        (Item_Base item, StackModSize size) => {
            int newSize;
            if (!size.Percentage)
                newSize = size.Size;
            else
                newSize = size.Size / 100 * item.settings_Inventory.StackSize;
            Traverse.Create(item.settings_Inventory).Field("stackSize").SetValue(newSize);
        };

    // <uniqueName_of_item or _, size>
    // _ = all others
    private readonly Dictionary<string, StackModSize> sizes = new Dictionary<string, StackModSize>();
    
    public void Start() {
        registerCommands();

        try {
            if (!StackModFileHandler.DefaultFileExists) {
                StackModFileHandler.SaveDefaultSizesIfNotExists(
                    ItemManager.GetAllItems().FindAll(predicate).ToDictionary(
                        item => item.UniqueName,
                        item => new StackModSize(item.UniqueName, item.settings_Inventory.StackSize)
                    )
                );
            }
        } catch(Exception ex) {
            Log(LogType.Error, "Failed saving default data. Restore after changing is perhaps be not possible.");
        }

        try {
            StackModFileHandler.LoadSizes().ToList().ForEach(kvp => sizes.Add(kvp.Value.UniqueName, kvp.Value));
        } catch(Exception ex) {
            Log(LogType.Error, "Failed to load the saved data:\n"+ex.Message+"\n"+ex.StackTrace);
        }
        AddBackwardsCompatibility(); // in a earlier version the PlayerPrefs Class was used. Take this value and use it in new solution

        if (sizes.Count > 0) {
            ApplyChangedSizes();
        }
        Log("StackMod has been loaded!");
    }

    private void registerCommands() {
        RConsole.registerCommand(typeof(StackMod), "", "stacksize", execCommand);
        RConsole.registerCommand(typeof(StackMod), "", "stacksizeof", debug);
        RConsole.registerCommand(typeof(StackMod), "", "searchItemIdentifier", execCommand);

    }

    private void debug() {
        string command = RConsole.lcargs[0];
        string[] param = RConsole.lcargs.Skip(1).ToArray();
        param.ToList().ForEach(itemname => RConsole.Log("> " + ItemManager.GetItemByName(itemname).settings_Inventory.DisplayName + " : " + ItemManager.GetItemByName(itemname).settings_Inventory.StackSize));
    }

    private void AddBackwardsCompatibility() {
        int lastSize = PlayerPrefs.GetInt("lastSize", 0);
        if (lastSize > 0) {
            sizes["_"] = new StackModSize("_", lastSize);
            PlayerPrefs.DeleteKey("lastSize");
            try {
                StackModFileHandler.SaveSizes(sizes);
            } catch(Exception ex) {
                Log(LogType.Error, "Failed saving the data");
            }
        }
    }

    void ApplyChangedSizes() {
        StackModSize size;

        Log("Changing sizes");
        RestoreItemStacksizes();

        List<Item_Base> items = ItemManager.GetAllItems();
        foreach (Item_Base item in items.FindAll(predicate)) {
            if (sizes.TryGetValue(item.UniqueName, out size)) {
                setStackSize.Invoke(item, size);
            } else if (sizes.TryGetValue("_", out size)) {
                setStackSize.Invoke(item, size);
            }
        }

        Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(items);
    }

    private string[] parseCommandLine(string arguments) {
        return Regex.Matches(arguments, @"[\""].+?[\""]|[^ ]+")
            .Cast<Match>()
            .Select(x => x.Value.Trim('"'))
            .ToArray();
    }

    void execCommand() {
        string command = RConsole.lcargs[0];
        string[] param = parseCommandLine(string.Join(" ", RConsole.lcargs.Skip(1).ToArray()));

        switch (command) {
            case "searchItemIdentifier":
                string searchTerm = string.Join(" ", param);
                string[] searchResult = getItemsByDisplayNamePart(searchTerm);
                if(searchResult.Length == 0) {
                    Log("No search results found for '"+ searchTerm + "'!");
                } else {
                    Log("Found Items:");
                    searchResult.ToList().ForEach(str => Log("- "+str));
                }
                break;
            case "stacksize":
                switch (param.Length) {
                    case 0:
                        PrintHelp();
                        PrintActualValues();
                        break;
                    case 1:
                        SetBaseStacksize(param[0]);
                        break;
                    default:
                        if(param[0] == "-" && param[1] == "all") {
                            RestoreItemStacksizes();
                            sizes.Clear();
                            StackModFileHandler.SaveSizes(sizes);
                        } else {
                            SetStacksizeOfItems(param[0], param.Skip(1).ToArray());
                        }
                        break;
                }
                break;
            default:
                Log(LogType.Error, "Unknown command. Should never be reached!");
                break;
        }
    }

    protected string[] getItemsByDisplayNamePart(string searchterm) {
        return ItemManager.GetAllItems().FindAll(
            item => predicate.Invoke(item) &&
            item.settings_Inventory.DisplayName.Contains(searchterm, StringComparison.OrdinalIgnoreCase)
        ).Select(
            item => item.settings_Inventory.DisplayName + " : " + item.UniqueName
        ).ToArray();
    }
    void SetBaseStacksize(string size) {
        if(size == "-") {
            // Restore default value
            sizes.Remove("_");
        } else if(size.EndsWith("%")) {
            sizes["_"] = new StackModSize("_", int.Parse(size.Substring(0, size.Length - 1)), true);
        } else {
            sizes["_"] = new StackModSize("_", int.Parse(size));
        }
        StackModFileHandler.SaveSizes(sizes);
        ApplyChangedSizes();
    }
    void SetStacksizeOfItems(string sizeStr, string[] itemnames) {
        itemnames.ToList().ForEach(itemname => {
            if (ItemManager.GetItemByName(itemname)) {
                if (sizeStr == "-") {
                    // Restore default value
                    if(sizes.ContainsKey(itemname)) {
                        sizes.Remove(itemname);
                    } else {
                        Log(LogType.Warning, "Item '"+itemname+"' didn't had a value set. So nothing happens.");
                    }
                } else if (sizeStr.EndsWith("%")) {
                    sizes[itemname] = new StackModSize(itemname, int.Parse(sizeStr.Substring(0, sizeStr.Length - 1)), true);
                } else {
                    sizes[itemname] = new StackModSize(itemname, int.Parse(sizeStr));
                }
                StackModFileHandler.SaveSizes(sizes);
                ApplyChangedSizes();
            } else {
                Log(LogType.Warning, "Item '"+itemname+"' does not exists. Stacksize was not changed for this item.");
            }
        });
    }

    private void RestoreItemStacksizes() {
        try {
            StackModFileHandler.LoadDefaultSizes().ToList().ForEach(entry => setStackSize.Invoke(ItemManager.GetItemByName(entry.Value.UniqueName), entry.Value));
        }catch(Exception ex) {
            Log(LogType.Error, "Failed to load the saved default data");
        }
    }

    private void PrintHelp(string error = "") {
        var helpMessageLines = new List<string>
            {
                string.Empty,
                "Usage: <b>searchItemIdentifier <i>[part of name]</i></b>",
                string.Empty,
                "Usage: <b>stacksize <i>[number]</i></b>  # sets the stacksize of all items without specific set stacksizes",
                "Usage: <b>stacksize <i>[percentage]</i></b>  # increases the stacksize of all items without specific set stacksizes by X%",
                "Usage: <b>stacksize <i>[number]</i> <i>[item identifier1]</i> <i>[item identifier2]</i> <i>...</i></b>  # sets a specific stacksize for specific items",
                "Usage: <b>stacksize <i>[percentage]</i> <i>[item identifier1]</i> <i>[item identifier2]</i> <i>...</i></b>  # sets a specific stacksize for specific items",
                string.Empty,
                "Usage: <b>stacksize -</b>  # restores default value for all items that don't have specific set stacksizes",
                "Usage: <b>stacksize - <i>[item identifier1]</i> <i>[item identifier2]</i> <i>...</i></b>  # restores default stacksize for specific items",
                "Usage: <b>stacksize - all</b>  # restores all default values. DELETES ALL YOUR CUSTOM SETTINGS!",
                string.Empty,
            };

        helpMessageLines.ForEach(RConsole.Log);
    }
    private void PrintActualValues() {
        StackModSize allSize;
        if (sizes.TryGetValue("_", out allSize)) {
            RConsole.Log("All (for all items except items with specific set stacksizes) : " + allSize.Size + (allSize.Percentage ? "%" : ""));
        }
        foreach (KeyValuePair<string, StackModSize> entry in sizes) {
            if (entry.Key != "_") {
                RConsole.Log(ItemManager.GetItemByName(entry.Key).settings_Inventory.DisplayName + " : " + entry.Value.Size + (entry.Value.Percentage ? "%" : ""));
            }
        }
    }

    public void Update() {
    }

    public void OnModUnload() {
        RestoreItemStacksizes();
        Log("StackMod has been unloaded!");
        Destroy(gameObject);
    }
    public void Log(string message) {
        Log(LogType.Log, message);
    }
    public void Log(LogType lt, string message) {
        Dictionary<LogType, string> logTypeFormatMapper = new Dictionary<LogType, string>() {
            { LogType.Warning, "<color=yellow>{0} {1}</color>" },
            { LogType.Log, "{0} {1}" },
            { LogType.Assert, "<color=red>{0} {1}</color>" },
            { LogType.Error, "<color=red>{0} {1}</color>" },
            { LogType.Exception, "<color=red>{0} {1}</color>" }
        };
        string prefix = String.Format("[{0} v{1}]", this.GetType().GetCustomAttribute<ModTitle>().ModTitleAttribute, this.GetType().GetCustomAttribute<ModVersion>().ModVersionAttribute);
        string format;
        RConsole.Log(logTypeFormatMapper.TryGetValue(lt, out format) ? String.Format(format, prefix, message) : prefix + " " + message);
    }
}
public class StackModSize {
    private string uniqueName;
    private int size;
    private bool percentage;
    public string UniqueName { get { return uniqueName; } }
    public int Size { get { return size; } }
    public bool Percentage { get { return percentage; } }
    public StackModSize(string uniqueName, int size) : this(uniqueName, size, false) { }
    public StackModSize(string uniqueName, int size, bool percentage) {
        this.uniqueName = uniqueName;
        this.size = size;
        this.percentage = percentage;
    }
    public void setSize(string size) {
        int helper;
        if(!int.TryParse(size, out helper)) throw new ArgumentException();
        setSize(helper, size.EndsWith("%") ? true : false);
    }
    public void setSize(int size, bool percentage = false) {
        this.size = size;
        this.percentage = percentage;
    }
}
public class StackModFileHandler {
    private readonly static string dataPath = @"\mods\ModData\StackMod\data.txt";
    private readonly static string defaultDataPath = @"\mods\ModData\StackMod\defaults.txt";

    public static bool DefaultFileExists { get { return File.Exists(Directory.GetCurrentDirectory() + defaultDataPath); } }

    public static void SaveSizes(Dictionary<string, StackModSize> sizes) {
        try {
            if (!Directory.Exists(Path.GetDirectoryName(Directory.GetCurrentDirectory() + dataPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(Directory.GetCurrentDirectory() + dataPath));

            File.WriteAllLines(
                Directory.GetCurrentDirectory() + dataPath,
                sizes.Select(entry => String.Format("{0}:{1}", entry.Key, entry.Value.Size + (entry.Value.Percentage ? "%" : "")))
            );
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static Dictionary<string, StackModSize> LoadSizes() {
        try {
            if (!File.Exists(Directory.GetCurrentDirectory() + dataPath))
                return new Dictionary<string, StackModSize>();

            return File.ReadLines(Directory.GetCurrentDirectory() + dataPath).Select(line => line.Split(':'))
                .ToDictionary(
                    lineParts => lineParts[0],
                    lineParts => new StackModSize(lineParts[0], int.Parse(lineParts[1].Substring(0, lineParts[1].Length-1)), lineParts[1].EndsWith("%"))
                );
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static void SaveDefaultSizesIfNotExists(Dictionary<string, StackModSize> defaultSizes) {
        try {
            if (!Directory.Exists(Path.GetDirectoryName(Directory.GetCurrentDirectory() + defaultDataPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(Directory.GetCurrentDirectory() + defaultDataPath));

            File.WriteAllLines(
                Directory.GetCurrentDirectory() + defaultDataPath,
                defaultSizes.Select(entry => String.Format("{0}:{1}", entry.Value.UniqueName, entry.Value.Size))
            );
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static Dictionary<string, StackModSize> LoadDefaultSizes() {
        try {
            if (!File.Exists(Directory.GetCurrentDirectory() + defaultDataPath))
                return new Dictionary<string, StackModSize>();

            return File.ReadLines(Directory.GetCurrentDirectory() + defaultDataPath).Select(line => line.Split(':'))
                .ToDictionary(
                    lineParts => lineParts[0],
                    lineParts => new StackModSize(lineParts[0], int.Parse(lineParts[1]))
                );
        } catch (Exception ex) {
            throw ex;
        }
    }
}